#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail

_json_license_file=${1:?Make sure to pass the path to JSON license file as the first script arg.}

[[ -f "$_json_license_file" ]] || {
  printf "[ERROR] %s\n" "There is no file at given path: '$_json_license_file'"
}

kubectl create secret generic eck-license --from-file="$_json_license_file" -n elastic-system
kubectl label secret eck-license "license.k8s.elastic.co/scope"=operator -n elastic-system