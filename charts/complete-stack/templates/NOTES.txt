You've installed the {{ .Chart.Name }}

Release name is {{ .Release.Name }}

### NOTE ####

It might take some time for resources to come up and configure (esp. if it's the first deployment)

#############

Ingress Urls (for access outside the cluster):
    APM Server: {{ include "chart.apm.ingressUrl" . }}
    ElasticSearch: {{ include "chart.es.ingressUrl" . }}
    Kibana: {{ include "chart.kb.ingressUrl" . }}
    Enterprise Search: {{ include "chart.ent.ingressUrl" . }}
    Maps Service: {{ include "chart.ems.ingressUrl" . }}
    Elastic Package Registry: {{ include "chart.epr.ingressUrl" . }}
    Elastic Artifact Registry: {{ include "chart.ear.ingressUrl" . }}
    Elastic Security Artifact Repository: {{ include "chart.esar.ingressUrl" . }}
    Fleet Server: {{ include "chart.fleet.ingressUrl" . }}


Cluster Urls (for access inside the cluster):
    APM Server: {{ include "chart.apm.clusterUrl" . }}
    ElasticSearch: {{ include "chart.es.clusterUrl" . }}
    Kibana: {{ include "chart.kb.clusterUrl" . }}
    Enterprise Search: {{ include "chart.ent.clusterUrl" . }}
    Maps Service: {{ include "chart.ems.clusterUrl" . }}
    Elastic Package Registry: {{ include "chart.epr.clusterUrl" . }}
    Elastic Artifact Registry: {{ include "chart.ear.clusterUrl" . }}
    Elastic Security Artifact Repository: {{ include "chart.esar.clusterUrl" . }}
    Fleet Server: {{ include "chart.fleet.clusterUrl" . }}


### Helpful Commands ###

# Get Elastic User Password:
> kubectl get secrets {{ printf "%s-es-elastic-user" (include "chart.es.fullName" .) }} -o go-template={{`'{{ .data.elastic | base64decode | printf "%s\n" }}'`}}

# Checking License Usage:
> kubectl -n {{ .Values.eckNamespace }} get configmap elastic-licensing -o yaml