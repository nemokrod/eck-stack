ARG BASE_IMAGE=python:3.10-alpine
FROM $BASE_IMAGE

RUN apk add bash
# dev customizations
RUN apk add vim nano emacs curl jq httpie

WORKDIR /app

COPY . .
RUN ./scripts/setup_venv.sh

ENV APP_HOST=0.0.0.0
ENV APP_PORT=5000

# set these to enable APM
ENV ELATIC_APM_ENABLED "False"
ENV ELASTIC_APM_VERIFY_SERVER_CERT ""
ENV ELASTIC_APM_SERVICE_NAME ""
ENV ELASTIC_APM_SERVER_URL ""

EXPOSE 5000

CMD ["bash", "./scripts/start_dev.sh"]
