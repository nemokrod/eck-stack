#!/usr/bin/env bash
set -CEfue
set -o pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
APP_REPO_DIR=$( cd "${SCRIPT_DIR}/../" && pwd )

DOCKER_CMD="${DOCKER_CMD:-podman}"
DOCKERFILE="${DOCKERFILE:-prod.Dockerfile}"

BUILD_CTX="${APP_REPO_DIR}"
BUILD_IMAGE_NAME=$(basename "$APP_REPO_DIR")
BUILD_IMAGE_TAG="${BUILD_TAG:-latest}"
BUILD_ARCH="amd64"

PULL_REGISTRY="${PULL_REGISTRY:-docker.io}"
PUSH_REGISTRY="${PUSH_REGISTRY:?Set PUSH_REGISTRY var, and don\'t forget the port number}"

BUILD_BASE_IMAGE="${BUILD_BASE_IMAGE:-${PULL_REGISTRY}/python:3.10-alpine}"

printf "[INFO] %s\n" "Autheticating to registries ..."
for _registry in "$PULL_REGISTRY" "$PUSH_REGISTRY"; do
    printf "[INFO] %s\n" "Logging into "$PULL_REGISTRY""
    "$DOCKER_CMD" login "$_registry" --tls-verify=false
done

printf "[INFO] %s\n" "Starting build ..."
"$DOCKER_CMD" build \
    --file "${APP_REPO_DIR}/${DOCKERFILE}" \
    --arch "${BUILD_ARCH}" \
    --tag "${BUILD_IMAGE_NAME}:${BUILD_IMAGE_TAG}" \
    --build-arg "BASE_IMAGE=${BUILD_BASE_IMAGE}" \
    --tls-verify=false \
    "${BUILD_CTX}"
printf "[INFO] %s\n" "Tagging images ..."
"$DOCKER_CMD" tag \
    "${BUILD_IMAGE_NAME}:${BUILD_IMAGE_TAG}" \
    "${PUSH_REGISTRY}/${BUILD_IMAGE_NAME}:${BUILD_IMAGE_TAG}"
printf "[INFO] %s\n" "Pushing images ..."
"$DOCKER_CMD" push --tls-verify=false \
    "${PUSH_REGISTRY}/${BUILD_IMAGE_NAME}:${BUILD_IMAGE_TAG}"

