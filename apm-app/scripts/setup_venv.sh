#!/usr/bin/env bash
set -CEfue
set -o pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
APP_ROOT_DIR=$( cd "${SCRIPT_DIR}/../" &> /dev/null && pwd )
APP_REQUIREMENTS_TXT="${APP_ROOT_DIR}/requirements.txt"

VENV_DIR="${APP_ROOT_DIR}/venv"
VENV_PYTHON_CMD="${VENV_DIR}/bin/python"

PYTHON_CMD=${PYTHON_CMD:-python3}
REPLACE_VENV=${REPLACE_VENV:-no}


if [[ ${REPLACE_VENV,,} =~ ^yes$ ]]; then
    if [[ -d "$VENV_DIR" ]]; then
        rm -rf "$VENV_DIR"
    fi
fi

"$PYTHON_CMD" -m venv --copies --upgrade-deps "$VENV_DIR"
"$VENV_PYTHON_CMD" -m pip install wheel
"$VENV_PYTHON_CMD" -m pip install -r "$APP_REQUIREMENTS_TXT"
