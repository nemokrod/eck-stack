from flask import Flask
from elasticapm.contrib.flask import ElasticAPM

# disabling urllib3 warnings to declutter logs when using unverified TLS
import urllib3
urllib3.disable_warnings()

app = Flask(__name__)
# various ELASTIC_APM_<XXX> env vars can be used to configure APM
# ELASTIC_APM_ENABLED=<True/False> toggles whether APM instrumentation runs at all
apm = ElasticAPM(app)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/ping')
def ping():
    return 'pong!'


if __name__ == "__main__":
    print("Use <app_root>/scripts/start_[dev|prod].sh to launch api")