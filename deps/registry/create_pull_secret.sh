#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail

REGISTRY_URL="${REGISTRY_URL:-https://dregistry.aws.estorm-eck-lab.local}"
REGISTRY_USER="${REGISTRY_USER:-dregistry_user}"
REGISTRY_PASS="${REGISTRY_PASS:-dregistry_pass}"

PULL_SECRET_NAME="${PULL_SECRET_NAME:-dregistry-auth}"
PULL_SECRET_NAMESPACE="${PULL_SECRET_NAMESPACE:-default}"

kubectl create secret docker-registry "$PULL_SECRET_NAME" \
  --namespace="${PULL_SECRET_NAMESPACE}" \
  --docker-server="${REGISTRY_URL}" \
  --docker-username="${REGISTRY_USER}" \
  --docker-password="${REGISTRY_PASS}"

