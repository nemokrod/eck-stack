{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default $.Chart.Name $.Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chart.fullName" -}}
{{- if $.Values.fullnameOverride }}
{{- $.Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default $.Chart.Name $.Values.nameOverride }}
{{- if contains $name $.Release.Name }}
{{- $.Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" $.Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart.chart" -}}
{{- printf "%s-%s" $.Chart.Name $.Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "chart.labels" -}}
"helm.sh/chart": {{ include "chart.chart" $ | quote }}
{{- if $.Chart.AppVersion }}
"app.kubernetes.io/version": {{ $.Chart.AppVersion | quote }}
{{- end }}
"app.kubernetes.io/managed-by": {{ $.Release.Service | quote }}
{{- end }}

{{/*
    Registry Deployment/STS Params
*/}}
{{- define "chart.registry.name" -}}
{{ printf "%s" $.Release.Name }}
{{- end -}}
{{- define "chart.registry.labels" -}}
"app": {{ include "chart.registry.name" $ | quote }}
{{- end -}}

{{- define "chart.registry.image" -}}
{{- with $image_tag := default "latest" $.Values.image.tag -}}
{{- with $image_repo := trimSuffix "/" $.Values.image.repository -}}
{{ printf "%s:%s" $image_repo $image_tag }}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "chart.passgen.image" -}}
{{- with $image_tag := default "latest" $.Values.passgenImage.tag -}}
{{- with $image_repo := trimSuffix "/" $.Values.passgenImage.repository -}}
{{ printf "%s:%s" $image_repo $image_tag }}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "chart.registry.checksumAnnotations" -}}
"checksum/registry-config": {{ include (print $.Template.BasePath "/config.yaml") $ | sha256sum | quote }}
"checksum/registry-auth": {{ include (print $.Template.BasePath "/auth.yaml") $ | sha256sum | quote }}
"checksum/registry-tls-certs": {{ include (print $.Template.BasePath "/certs/cert.yaml") $ | sha256sum | quote }}
{{- end -}}

{{/*
    RBAC Params
*/}}
{{- define "chart.registry.serviceAccountName" -}}
{{ printf "%s" (include "chart.registry.name" $) }}
{{- end -}}

{{/*
    Cert Params
*/}}
{{- define "chart.certName" -}}
{{ printf "%s-cert" (include "chart.registry.name" $) }}
{{- end -}}
{{- define "chart.certIssuerName" -}}
{{ printf "%s-issuer" (include "chart.registry.name" $) }}
{{- end -}}
{{- define "chart.certReaderRoleName" -}}
{{ printf "%s-cert-reader" (include "chart.registry.name" $) }}
{{- end -}}

{{/*
    Config Params
*/}}
{{- define "chart.registry.auth.secretName" -}}
{{ printf "%s-auth" (include "chart.registry.name" $) }}
{{- end -}}

{{/*
    Secret Params
*/}}
{{- define "chart.registry.configName" -}}
{{ printf "%s-config" (include "chart.registry.name" $) }}
{{- end -}}

{{/*
    Service Params
*/}}
{{- define "chart.registry.service.name" -}}
{{ printf "%s" (include "chart.registry.name" $) }}
{{- end -}}
{{- define "chart.registry.service.port" -}}
443
{{- end -}}
{{- define "chart.registry.service.fqdn" -}}
{{ printf "%s.%s.svc" (include "chart.registry.service.name" $) $.Release.Namespace }}
{{- end -}}
{{- define "chart.registry.service.url" -}}
{{ printf "https://%s:%s" (include "chart.registry.service.fqdn" $) (include "chart.registry.service.port" .) }}
{{- end -}}


{{/*
    Ingress Params
*/}}
{{- define "chart.ingress.name" -}}
{{ printf "%s" (include "chart.registry.name" $) }}
{{- end -}}
{{- define "chart.ingress.fqdn" -}}
{{ $.Values.ingress.fqdn | required "Set .Values.ingress.fqdn" }}
{{- end -}}
{{- define "chart.ingress.url" -}}
{{ printf "https://%s" (include "chart.ingress.fqdn" $) }}
{{- end -}}
{{- define "chart.ingress.checksumAnnotations" -}}
"checksum/registry-tls-certs": {{ include (print $.Template.BasePath "/certs/cert.yaml") $ | sha256sum | quote }}
{{- end -}}


