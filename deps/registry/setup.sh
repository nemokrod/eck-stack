#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#####
## ## Docs:
#####   - Chart: <local>
  ###   - API: https://docs.docker.com/registry/spec/api/
   ##   - Deploy Docs: https://docs.docker.com/registry/deploying/
    #

# install conf
CHART_NAME="$SCRIPT_DIR/docker-registry"
RELEASE_NAME="docker-registry"
NAMESPACE="docker-registry"
ENV_VALUES="$SCRIPT_DIR/local.values.yaml"

REGISTRY_INGRESS_FQDN="${REGISTRY_INGRESS_FQDN:-dregistry.aws.estorm-eck-lab.local}"
REGISTRY_STORAGE_SIZE="${REGISTRY_STORAGE_SIZE:-100Gi}"
REGISTRY_AUTH_USERNAME="${REGISTRY_AUTH_USERNAME:-dregistry_user}"
REGISTRY_AUTH_PASSWORD="${REGISTRY_AUTH_PASSWORD:-dregistry_pass}"

# install
helm upgrade --install \
    "$RELEASE_NAME" "$CHART_NAME" \
    --namespace "$NAMESPACE" \
    --create-namespace \
    --values "$ENV_VALUES" \
    --set ingress.fqdn="${REGISTRY_INGRESS_FQDN}" \
    --set registrySize="${REGISTRY_STORAGE_SIZE}" \
    --set auth.username="${REGISTRY_AUTH_USERNAME}" \
    --set auth.password="${REGISTRY_AUTH_PASSWORD}"
