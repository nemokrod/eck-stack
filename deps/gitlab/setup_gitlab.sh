#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#####
## ## Docs:
#####   - https://docs.gitlab.com/charts/installation/deployment.html
  ###
   ##
    #

# install conf
LOCAL_HELM_REPO_NAME="gitlab"
REMOTE_HELM_REPO_URL="https://charts.gitlab.io"
CHART_NAME="$LOCAL_HELM_REPO_NAME/gitlab"
RELEASE_NAME="gitlab"
NAMESPACE="gitlab"
ENV_VALUES="$SCRIPT_DIR/gitlab.values.yaml"

# setup helm
helm repo add "$LOCAL_HELM_REPO_NAME" "$REMOTE_HELM_REPO_URL"
helm repo update

# install
helm upgrade --install \
    "$RELEASE_NAME" "$CHART_NAME" \
    --namespace "$NAMESPACE" \
    --create-namespace \
    --values "$ENV_VALUES" \
    --timeout 600s \
    --set global.hosts.domain="${GITLAB_ROOT_DOMAIN:?Missing env var}" \
    --set global.hosts.externalIP="${GITLAB_INGRESS_IP:?Missing env var}" \
    --set postgresql.image.tag=13.6.0 \
    --set global.edition=ce \
    --set certmanager.install=false \
    --set nginx-ingress.enabled=false \
    --set global.ingress.class=nginx \
    --set global.ingress.configureCertmanager=false \
    --set global.kas.enabled=true \
    --set gitlab-runner.certsSecretName="${RELEASE_NAME}-wildcard-tls-chain"