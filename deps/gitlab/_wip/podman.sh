#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

GITLAB_REGISTRY_FQDN="${GITLAB_REGISTRY_FQDN:-registry.aws.estorm-eck-lab.local}"
GITLAB_MIRROR_PROJECT="elastic/docker.io"
CONTAINER_IMAGE_TO_SYNC="python:3.10-alpine"
CONTAINER_IMAGE_IN_GITLAB="${GITLAB_REGISTRY_FQDN}/${GITLAB_MIRROR_PROJECT}/${CONTAINER_IMAGE_TO_SYNC}"

podman login "$GITLAB_REGISTRY_FQDN" --tls-verify=false
# You'll be prompted for user/pass
#   - use gitlab login creds, same as ones for Gitlab UI
podman pull "$CONTAINER_IMAGE_TO_SYNC"
podman tag "$CONTAINER_IMAGE_TO_SYNC" "$CONTAINER_IMAGE_IN_GITLAB"
podman push "$CONTAINER_IMAGE_IN_GITLAB" --tls-verify=false
