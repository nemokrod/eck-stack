#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#####
## ## Docs:
#####   - https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-state-metrics#configuration
  ###
   ##
    #

# install conf
LOCAL_HELM_REPO_NAME="prometheus-community"
REMOTE_HELM_REPO_URL="https://prometheus-community.github.io/helm-charts"
CHART_NAME="$LOCAL_HELM_REPO_NAME/kube-state-metrics"
RELEASE_NAME="kube-state-metrics"
NAMESPACE="default"
ENV_VALUES="$SCRIPT_DIR/local.values.yaml"

# setup helm
helm repo add "$LOCAL_HELM_REPO_NAME" "$REMOTE_HELM_REPO_URL"
helm repo update

# install
helm upgrade --install \
    "$RELEASE_NAME" "$CHART_NAME" \
    --namespace "$NAMESPACE" \
    --create-namespace \
    --values "$ENV_VALUES"