At the time of this writing the only Elastic option to build inside the K8 cluster is ECK. To follow the ECK instructions [click here to get to the README.md](./eck/README.md)
