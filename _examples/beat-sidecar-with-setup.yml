apiVersion: apps/v1
kind: Deployment
metadata:
  name: example-app
  namespace: default
  labels:
    app: example-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: example-app
  template:
    metadata:
      name: example-app
      labels:
        app: example-app
    spec:
      serviceAccountName: filebeat-sidecar
      initContainers:
        - name: filebeat-setup
          image: docker.elastic.co/beats/filebeat:8.4.3
          args:
            - "setup"
            - "-c"
            - "/etc/filebeat.yml"
            - "-e"
          volumeMounts:
            - name: app-logs
              mountPath: /var/log/nginx/
            - name: filebeat-config
              mountPath: /etc/filebeat.yml
              readOnly: true
              subPath: filebeat.yml
          env:
          # check this to find out which fields can be projected to env vars: https://kubernetes.io/docs/concepts/workloads/pods/downward-api/#available-fields
            - name: K8S_NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: K8S_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: K8S_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: K8S_POD_UID
              valueFrom:
                fieldRef:
                  fieldPath: metadata.uid
            - name: K8S_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          envFrom:
            - secretRef:
                name: es-credentails-for-beats
          resources:
            requests:
              cpu: 100m
              memory: 100Mi
            limits:
              cpu: 1000m
              memory: 256Mi
      containers:
        - name: example-app
          image: nginx:1.23.1
          ports:
            - containerPort: 80
              name: http
          volumeMounts:
            - name: app-logs
              mountPath: /var/log/nginx/
          resources:
            requests:
              cpu: 125m
              memory: 128Mi
            limits:
              cpu: 1000m
              memory: 256Mi
        - name: filebeat-sidecar
          image: docker.elastic.co/beats/filebeat:8.3.3
          args:
            - "-c"
            - "/etc/filebeat.yml"
            - "-e"
          volumeMounts:
            - name: app-logs
              mountPath: /var/log/nginx/
            - name: filebeat-config
              mountPath: /etc/filebeat.yml
              readOnly: true
              subPath: filebeat.yml
          env:
          # check this to find out which fields can be projected to env vars: https://kubernetes.io/docs/concepts/workloads/pods/downward-api/#available-fields
            - name: K8S_NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: K8S_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: K8S_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: K8S_POD_UID
              valueFrom:
                fieldRef:
                  fieldPath: metadata.uid
            - name: K8S_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          envFrom:
            - secretRef:
                name: es-credentails-for-beats
          resources:
            requests:
              cpu: 100m
              memory: 100Mi
            limits:
              cpu: 1000m
              memory: 256Mi
      volumes:
        - name: app-logs
          emptyDir: {}
        - name: filebeat-config
          configMap:
            defaultMode: 0640
            name: example-app-filebeat-config
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: example-app-filebeat-config
  namespace: default
data:
  filebeat.yml: |-
    filebeat.inputs:
      - type: log
        paths:
          - /var/log/nginx/*.log*
    processors:
      - add_fields:
          target: ''
          fields:
            # set following conventions in https://www.elastic.co/guide/en/beats/filebeat/current/exported-fields-kubernetes-processor.html
            kubernetes.pod.name: '${K8S_POD_NAME}'
            kubernetes.pod.id: '${K8S_POD_UID}'
            kubernetes.pod.ip: '${K8S_POD_IP}'
            kubernetes.namespace: '${K8S_NAMESPACE}'
            kubernetes:node_name: '${K8S_NODE_NAME}'

    setup.kibana:
      host: '${KIBANA_HOST:kibana}:${KIBANA_PORT:5601}'
      ssl.verification_mode: 'none'
      username: '${KIBANA_USERNAME}'
      password: '${KIBANA_PASSWORD}'
    output.elasticsearch:
      hosts: ['${ELASTICSEARCH_HOST:elasticsearch}:${ELASTICSEARCH_PORT:9200}']
      username: '${ELASTICSEARCH_USERNAME}'
      password: '${ELASTICSEARCH_PASSWORD}'
      ssl.verification_mode: 'none'
---
apiVersion: v1
kind: Secret
metadata:
  name: es-credentails-for-beats
  namespace: default
type: Opaque
stringData:
  KIBANA_HOST: "https://demo-0.kb.us-gov-east-1.aws.elastic-cloud.com"
  KIBANA_PORT: "9243"
  KIBANA_USERNAME: "elastic"
  KIBANA_PASSWORD: "changepass"
  ELASTICSEARCH_HOST: "https://demo-0.es.us-gov-east-1.aws.elastic-cloud.com"
  ELASTICSEARCH_PORT: "443"
  ELASTICSEARCH_USERNAME: "elastic"
  ELASTICSEARCH_PASSWORD: "changepass"
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: es-credentials-for-beats
  namespace: default
rules:
- apiGroups:
  - ""
  resources:
  - secrets
  resourceNames:
  - es-credentials-for-beats
  verbs:
  - get
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: es-credentials-for-beats
  namespace: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: es-credentials-for-beats
subjects:
- kind: ServiceAccount
  name: filebeat-sidecar
  namespace: default
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: filebeat-sidecar
  namespace: default
automountServiceAccountToken: true
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: example-app
  name: example-app
spec:
  ports:
  - port: 80
    name: http
    protocol: TCP
    targetPort: http
  selector:
    app: example-app
---
apiVersion:  networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-app
  namespace: default
spec:
  ingressClassName: nginx
  rules:
  - host: app.lab.local
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: example-app
            port:
              name: http
